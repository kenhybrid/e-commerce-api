// models and package import
import nodemailer from "nodemailer";
import exist from "email-existence";
import "dotenv/config.js";
import { BadRequest } from "http-errors";
// prepare transporter
const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  secure: true, // use SSL
  auth: {
    user: process.env.EMAIL,
    pass: process.env.PASSWORD,
  },
});
const WellcomeMailService = async (data) => {
  try {
    const { email, name, slug } = data;
    const mailOptions = {
      from: "Shoppy" + `<${process.env.EMAIL}>`, // sender address (who sends)
      to: email, // list of receivers (who receives)
      subject: `ACCOUNT VERIFICATION. `, // Subject line
      html: `<b>Dear ${name} </b> <br>
      <a href="http://localhost:8000/auth/verify/${slug}">Verify your account and start shopping with us</a>
      `,
    };

    // send mail
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
      }

      console.log("Message sent: " + info.response);
    });
  } catch (error) {
    return error;
  }
};

module.exports = {
  WellcomeMailService,
};
