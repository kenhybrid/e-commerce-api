// models and package import
import User from "../models/User.js";
import { HashPassword, ComparePasswordHash } from "../helpers/hashing.js";
import { GenerateRandomBytes } from "../helpers/randomize.js";
import { NotFound, Unauthorized, BadRequest, Conflict } from "http-errors";
const RegisterClientService = async (data) => {
  try {
    const { email, password, name, phone } = data;
    // is user registered by email
    const emailIsRegistered = await User.findOne({ email: email });
    if (emailIsRegistered) throw Conflict("Ooops! Email is already taken");
    // hash passwords
    const hash = await HashPassword(password);
    const slug = await GenerateRandomBytes();
    // save user
    const userDetails = new User({
      email: email,
      name: name,
      phone: "+"+phone,
      password: hash,
      slug: slug,
    });
    const doc = await userDetails.save();
    return doc;
  } catch (error) {
    return error;
  }
};

module.exports = {
  RegisterClientService,
};
