import crypto from "crypto";

const GenerateRandomBytes = async () => {
  return await crypto.randomBytes(4).toString("hex");
};
module.exports = {
  GenerateRandomBytes,
};
