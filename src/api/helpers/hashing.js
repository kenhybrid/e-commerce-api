import argon from "argon2";

const HashPassword = async (password) => {
  const hash = await argon.hash(password);
  return hash;
};

const ComparePasswordHash = async (hash, password) => {
  const passwordMatch = await argon.verify(hash, password);
  return passwordMatch;
};

module.exports = {
  HashPassword,
  ComparePasswordHash,
};
