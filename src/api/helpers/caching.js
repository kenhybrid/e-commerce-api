import redis from "redis";

const client = redis.createClient({
  port: 6379,
  host: "127.0.0.1",
});

client.on("connect", () => {
  console.log("Redis connection is on");
});

client.on("ready", () => {
  // console.log("Redis is redy to use");
});

client.on("error", (err) => {
  console.log(err.message);
});

client.on("end", () => {
  console.log("Redis is disconnected");
});

process.on("SIGINT", () => {
  client.quit();
});

module.exports = client;

