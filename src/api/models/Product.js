import mongoose from "mongoose";
//creating a products schema
const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    brand: {
      type: String,
      required: true,
    },
    category: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    quantity: {
      type: Number,
      required: true,
    },
    image: {
      type: String,
    },
  },
  { timestamps: true }
);
// product search
productSchema.index(
  { name: "text", description: "text" },
  { weights: { name: 10, description: 5 } }
);

module.exports = mongoose.model("products", productSchema);
