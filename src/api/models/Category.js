import mongoose from "mongoose";
import Product from "./Product";
//creating a categoriess schema
const categoriesSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    products: [{ type: mongoose.Schema.Types.ObjectId, ref: Product }],
  },
  { timestamps: true }
);

module.exports = mongoose.model("categories", categoriesSchema);
