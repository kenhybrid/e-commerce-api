import mongoose from "mongoose";
import Product from "./Product";
//creating a brands schema
const brandSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    image: {
      type: String,
    },
    products: [{ type: mongoose.Schema.Types.ObjectId, ref: Product }],
  },
  { timestamps: true }
);

module.exports = mongoose.model("brands", brandSchema);
