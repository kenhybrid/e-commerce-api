import {
  CreateProduct,
  GetAllProducts,
  GetOneProduct,
  DeleteProduct,
  UpdateProduct,
  SearchProducts,
  BrandProducts,
  CategoryProducts,FilterProducts
} from "../controllers/productsController.js";

const route = async (app, opts, done) => {
  app.route({
    method: "POST",
    url: "/create",
    handler: CreateProduct,
  });
  app.route({
    method: "POST",
    url: "/search",
    handler: SearchProducts,
  });
  app.route({
    method: "POST",
    url: "/sort",
    handler: FilterProducts,
  });
  app.route({
    method: "GET",
    url: "/all",
    handler: GetAllProducts,
  });
  app.route({
    method: "GET",
    url: "/one/:id",
    handler: GetOneProduct,
  });
  app.route({
    method: "DELETE",
    url: "/delete/:id",
    handler: DeleteProduct,
  });
  app.route({
    method: "PUT",
    url: "/update/:id",
    handler: UpdateProduct,
  });
  app.route({
    method: "GET",
    url: "/brandproducts",
    handler: BrandProducts,
  });
  app.route({
    method: "GET",
    url: "/categoryproducts",
    handler: CategoryProducts,
  });
  done();
};

module.exports = route;
