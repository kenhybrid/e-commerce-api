import {
  CreateCategory,
  DeleteCategory,
  UpdateCategory,
  GetAllCategories,
} from "../controllers/categoriesController.js";

const route = async (app, opts, done) => {
  app.route({
    method: "POST",
    url: "/create",
    handler: CreateCategory,
  });
  app.route({
    method: "GET",
    url: "/all",
    handler: GetAllCategories,
  });
  app.route({
    method: "DELETE",
    url: "/delete/:id",
    handler: DeleteCategory,
  });
  app.route({
    method: "PUT",
    url: "/update/:id",
    handler: UpdateCategory,
  });
  done();
};

module.exports = route;
