import {
  CreateBrand,
  DeleteBrand,
  UpdateBrand,
  GetAllBrands,
} from "../controllers/brandsController";

const route = async (app, opts, done) => {
  app.route({
    method: "POST",
    url: "/create",
    handler: CreateBrand,
  });
  app.route({
    method: "GET",
    url: "/all",
    handler: GetAllBrands,
  });
  app.route({
    method: "DELETE",
    url: "/delete/:id",
    handler: DeleteBrand,
  });
  app.route({
    method: "PUT",
    url: "/update/:id",
    handler: UpdateBrand,
  });
  done();
};

module.exports = route;
