// import models and validation
import Product from "../models/Product.js";
import Category from "../models/Category.js";
import Brand from "../models/Brand.js";
import { Conflict, NotFound } from "http-errors";
import {
  ProductCreationValidation,
  ProductFilterValidation,
  ProductUpdateValidation,
  ProductSearchValidation,
} from "../validations/productsValidation.js";
import redis from "../helpers/caching.js";
// controllers
const CreateProduct = async (req, res) => {
  try {
    const data = await ProductCreationValidation.validateAsync(req.body);
    const { name, brand, category, price, quantity, description } = data;
    // check if brand or category exists
    const brandExists = await Brand.findOne({ _id: brand });
    if (!brandExists) throw Conflict("brand not found");
    const categoryExists = await Category.findOne({ _id: category });
    if (!categoryExists) throw Conflict("category not found");
    // product creation
    const product = await new Product({
      name: name,
      brand: brand,
      category: category,
      price: price,
      quantity: quantity,
      description: description,
    });
    const doc = await product.save();
    // updating brand relationship
    await Brand.updateOne(
      { _id: doc.brand },
      { $push: { products: product._id } }
    );
    // updating category relationship
    await Category.updateOne(
      { _id: doc.category },
      { $push: { products: product._id } }
    );
    res.send({ message: "success", doc: doc });
  } catch (error) {
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const GetAllProducts = (req, res) => {
  // caching
  const key = req.url;
  redis.get(key, async (err, data) => {
    try {
      if (err) throw err;
      if (data == null) {
        // pagination function
        const limit = 6;
        const page =
          parseInt(req.query.page) >= 0 ? parseInt(req.query.page) : 1;
        const pagination = {};
        const endIndex = page * limit;
        const startIndex = (page - 1) * limit;
        const count = await Product.countDocuments().exec();
        let allPages;
        // getting all pages
        if ((await Product.countDocuments().exec()) % limit > 0) {
          allPages = parseInt(
            (await Product.countDocuments().exec()) / limit + 1
          );
        } else {
          allPages = (await Product.countDocuments().exec()) / limit;
        }
        // assigning pagination config
        pagination.pages = allPages;
        pagination.limit = limit;
        pagination.count = count;

        // pagination logic
        if (endIndex < count) {
          // show next pagination
          pagination.next = {
            page: page + 1,
          };
        }
        if (startIndex > 0) {
          pagination.previous = {
            page: page - 1,
          };
        }
        // getting paginated products
        const products = await Product.find({})
          .select("name description image price")
          .sort({ createdAt: "asc" })
          .limit(limit)
          .skip(startIndex);
        res.send({ products, pagination });
        console.log("from db");
        const payload = {
          products: products,
          pagination: pagination,
        };
        // add to cache
        await redis.set(key, JSON.stringify(payload), "EX", 3 * 60);
      } else {
        res.send(await JSON.parse(data));
        console.log("from cache");
      }
    } catch (error) {
      const error = {
        status: error.status,
        message: error.message,
      };
      res.send({ error: error });
    }
  });
};
const GetOneProduct = (req, res) => {
  // caching
  const key = req.url;
  const { id } = req.params;
  redis.get(key, async (err, data) => {
    try {
      if (err) throw err;
      if (data == null) {
        // getting paginated products
        const products = await Product.findOne({ _id: id }).select(
          "name description image price quantity"
        );
        if (products == null) throw NotFound("product not found");
        res.send({ products });
        const payload = {
          products: products,
        };
        await redis.set(key, JSON.stringify(payload), "EX", 3 * 60);
      } else {
        res.send(await JSON.parse(data));
        console.log("from cache");
      }
    } catch (error) {
      const error = {
        status: error.status,
        message: error.message,
      };
      res.send({ error: error });
    }
  });
};
const FilterProducts = async (req, res) => {
  try {
    const data = await ProductFilterValidation.validateAsync(req.body);
    const { category, brand, pricelow,pricehigh } = data;
    if (category && brand) {
      // filter all
    }  else if(brand) {
      // filter by brand
    }else if(category) {
      //  filter by category
    }else{
      // filter by price
    }
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const DeleteProduct = async (req, res) => {
  try {
    const { id } = req.params;
    const product = await Product.findOne({ _id: id });
    if (!product) throw NotFound("product not found");
    // update category population
    await Category.updateOne(
      { _id: product.category },
      { $pull: { products: product._id } }
    );
    // update brand population
    await Brand.updateOne(
      { _id: product.brand },
      { $pull: { products: product._id } }
    );
    // delete product
    await Product.deleteOne({ _id: id });
    res.send({ message: "success" });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const UpdateProduct = async (req, res) => {
  try {
    const { id } = req.params;
    const data = await ProductUpdateValidation.validateAsync(req.body);
    const { name, brand, category, price, quantity, description } = data;
    // check for product existence
    const product = await Product.findById({ _id: id });
    if (!product) throw NotFound("product not found.");
    // update
    const update = {
      name: name || product.name,
      description: description || product.description,
      price: price || product.price,
      quantity: quantity || product.quantity,
      brand: brand || product.brand,
      category: category || product.category,
    };
    await Product.updateOne({ _id: id }, update);
    //  update category if changes
    if (category) {
      if (product.category != category) {
        // remove product id from initial category
        await Category.updateOne(
          { _id: product.category },
          { $pull: { products: product._id } }
        );
        // push product in new category
        await Category.updateOne(
          { _id: category },
          { $push: { products: product._id } }
        );
      }
    }
    //  update category if changes
    if (brand) {
      if (product.brand != brand) {
        // remove product id from initial brand
        await Brand.updateOne(
          { _id: product.brand },
          { $pull: { products: product._id } }
        );
        // push product in new brand
        await Brand.updateOne(
          { _id: brand },
          { $push: { products: product._id } }
        );
      }
    }
    res.send({ message: "success" });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const SearchProducts = async (req, res) => {
  try {
    // validation
    const data = await ProductSearchValidation.validateAsync(req.body);
    const { searchTerm } = data;
    //  search a product
    const products = await Product.find({
      $or: [
        { name: new RegExp(searchTerm, "gi") },
        { short_description: new RegExp(searchTerm, "gi") },
      ],
    }).select("name  price image");
    // on success
    res.send({ products });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const BrandProducts = (req, res) => {
  // caching
  const { brand } = req.query;
  const key = req.url;
  redis.get(key, async (err, data) => {
    try {
      if (err) throw err;
      if (data == null) {
        // pagination function
        const limit = 6;
        const page =
          parseInt(req.query.page) >= 0 ? parseInt(req.query.page) : 1;
        const pagination = {};
        const endIndex = page * limit;
        const startIndex = (page - 1) * limit;
        const count = await Product.find()
          .where("brand")
          .equals(brand)
          .countDocuments();
        let allPages;
        // getting all pages
        if (
          (await Product.find().where("brand").equals(brand).countDocuments()) %
            limit >
          0
        ) {
          allPages = parseInt(
            (await Product.find()
              .where("brand")
              .equals(brand)
              .countDocuments()) /
              limit +
              1
          );
        } else {
          allPages =
            (await Product.find()
              .where("brand")
              .equals(brand)
              .countDocuments()) / limit;
        }
        // assigning pagination config
        pagination.pages = allPages;
        pagination.limit = limit;
        pagination.count = count;

        // pagination logic
        if (endIndex < count) {
          // show next pagination
          pagination.next = {
            page: page + 1,
          };
        }
        if (startIndex > 0) {
          pagination.previous = {
            page: page - 1,
          };
        }
        // getting paginated products
        const products = await Product.find({})
          .select("name description image price")
          .sort({ createdAt: "asc" })
          .where("brand")
          .equals(brand)
          .limit(limit)
          .skip(startIndex);
        res.send({ products, pagination });
        console.log("from db");
        const payload = {
          products: products,
          pagination: pagination,
        };
        // add to cache
        await redis.set(key, JSON.stringify(payload), "EX", 3 * 60);
      } else {
        res.send(await JSON.parse(data));
        console.log("from cache");
      }
    } catch (error) {
      const error = {
        status: error.status,
        message: error.message,
      };
      res.send({ error: error });
    }
  });
};
const CategoryProducts = (req, res) => {
  // caching
  const { category } = req.query;
  const key = req.url;
  redis.get(key, async (err, data) => {
    try {
      if (err) throw err;
      if (data == null) {
        // pagination function
        const limit = 6;
        const page =
          parseInt(req.query.page) >= 0 ? parseInt(req.query.page) : 1;
        const pagination = {};
        const endIndex = page * limit;
        const startIndex = (page - 1) * limit;
        const count = await Product.find()
          .where("category")
          .equals(category)
          .countDocuments();
        let allPages;
        // getting all pages
        if (
          (await Product.find()
            .where("category")
            .equals(category)
            .countDocuments()) %
            limit >
          0
        ) {
          allPages = parseInt(
            (await Product.find()
              .where("category")
              .equals(category)
              .countDocuments()) /
              limit +
              1
          );
        } else {
          allPages =
            (await Product.find()
              .where("category")
              .equals(category)
              .countDocuments()) / limit;
        }
        // assigning pagination config
        pagination.pages = allPages;
        pagination.limit = limit;
        pagination.count = count;

        // pagination logic
        if (endIndex < count) {
          // show next pagination
          pagination.next = {
            page: page + 1,
          };
        }
        if (startIndex > 0) {
          pagination.previous = {
            page: page - 1,
          };
        }
        // getting paginated products
        const products = await Product.find({})
          .select("name description image price")
          .sort({ createdAt: "asc" })
          .where("category")
          .equals(category)
          .limit(limit)
          .skip(startIndex);
        res.send({ products, pagination });
        console.log("from db");
        const payload = {
          products: products,
          pagination: pagination,
        };
        // add to cache
        await redis.set(key, JSON.stringify(payload), "EX", 3 * 60);
      } else {
        res.send(await JSON.parse(data));
        console.log("from cache");
      }
    } catch (error) {
      const error = {
        status: error.status,
        message: error.message,
      };
      res.send({ error: error });
    }
  });
};
// exporting
module.exports = {
  CreateProduct,
  GetAllProducts,
  GetOneProduct,
  FilterProducts,
  DeleteProduct,
  UpdateProduct,
  SearchProducts,
  BrandProducts,
  CategoryProducts,
};
