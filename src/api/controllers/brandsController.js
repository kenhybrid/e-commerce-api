// import models and validation
import Brand from "../models/Brand.js";
import {
  BrandCreationValidation,
  BrandUpdateValidation,
} from "../validations/brandValidation.js";
import redis from "../helpers/caching.js";

// controllers
const CreateBrand = async (req, res) => {
  try {
    const data = await BrandCreationValidation.validateAsync(req.body);
    const { name } = data;
    // product creation
    const brand = await new Brand({
      name: name,
    });
    const doc = await brand.save();
    res.send({ message: "success", doc: doc });
  } catch (error) {
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const DeleteBrand = async (req, res) => {
  // functionality to be discused
  try {
    const { id } = req.params;
    const brand = await Brand.findOne({ _id: id });
    if (!brand) throw NotFound("brand not found");
    // delete products
    if (brand.products.length >= 1) {
      await Product.deleteMany({
        _id: { $in: brand.products },
      });
    }
    // delete brand
    await Brand.deleteOne({ _id: id });
    res.send({ message: "success" });
  } catch (error) {
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const UpdateBrand = async (req, res) => {
  try {
    const { id } = req.params;
    // validation
    const data = await BrandUpdateValidation.validateAsync(req.body);
    const { name } = data;

    const brand = await Brand.findOne({ _id: id });
    if (!brand) throw NotFound("brand not found");
    // update a category
    await Brand.updateOne({ _id: id }, { name: name });
    // print success
    res.send({ message: "success" });
  } catch (error) {
    // error handling
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const GetAllBrands =  (req, res) => {
  // caching
  const key = req.url;
  redis.get(key, async (err, data) => {
    try {
      if (err) throw err;
      if (data == null) {
        const brands = await Brand.find({})
          .select("name image")
          .sort({ createdAt: "asc" });
        res.send({ brands });
        console.log("from db");
        const payload = {
          brands: brands,
        };
        // add to cache
        await redis.set(key, JSON.stringify(payload), "EX", 3 * 60);
      } else {
        res.send(await JSON.parse(data));
        console.log("from cache");
      }
    } catch (error) {
      const error = {
        status: error.status,
        message: error.message,
      };
      res.send({ error: error });
    }
  });
};
// exporting
module.exports = {
  CreateBrand,
  DeleteBrand,
  UpdateBrand,
  GetAllBrands,
};
// how to paginate populated products
