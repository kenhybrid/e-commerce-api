// import models and validation
import Product from "../models/Product.js";
import Category from "../models/Category.js";
import {
  CategoryCreationValidation,
  CategoryUpdateValidation,
} from "../validations/categoryValidation.js";
import redis from "../helpers/caching.js";

// controllers
const CreateCategory = async (req, res) => {
  try {
    const data = await CategoryCreationValidation.validateAsync(req.body);
    const { name } = data;
    // product creation
    const category = await new Category({
      name: name,
    });
    const doc = await category.save();
    res.send({ message: "success" });
  } catch (error) {
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const DeleteCategory = async (req, res) => {
  // functionality to be discused
  try {
    const { id } = req.params;
    const category = await Category.findOne({ _id: id });
    if (!category) throw NotFound("category not found");
    // delete products
    if (category.products.length >= 1) {
      await Product.deleteMany({
        _id: { $in: category.products },
      });
    }
    // delete category
    await Category.deleteOne({ _id: id });
    res.send({ message: "success" });
  } catch (error) {
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const UpdateCategory = async (req, res) => {
  try {
    const { id } = req.params;
    // validation
    const data = await CategoryUpdateValidation.validateAsync(req.body);
    const { name } = data;

    const category = await Category.findOne({ _id: id });
    if (!category) throw NotFound("category not found");
    // update a category
    await Category.updateOne({ _id: id }, { name: name });
    // print success
    res.send({ message: "success" });
  } catch (error) {
    // error handling
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
const GetAllCategories = (req, res) => {
  // caching
  const key = req.url;
  redis.get(key, async (err, data) => {
    try {
      if (err) throw err;
      if (data == null) {
        const categories = await Category.find({})
          .select("name")
          .sort({ createdAt: "asc" });
        res.send({ categories });
        console.log("from db");
        const payload = {
          categories: categories,
        };
        // add to cache
        await redis.set(key, JSON.stringify(payload), "EX", 3 * 60);
      } else {
        res.send(await JSON.parse(data));
        console.log("from cache");
      }
    } catch (error) {
      const error = {
        status: error.status,
        message: error.message,
      };
      res.send({ error: error });
    }
  });
};
// exporting
module.exports = {
  CreateCategory,
  DeleteCategory,
  UpdateCategory,
  GetAllCategories,
};
// how to paginate populated products
