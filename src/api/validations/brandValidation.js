import Joi from "@hapi/joi";

const BrandCreationValidation = Joi.object({
  name: Joi.string().required(),
});
const BrandUpdateValidation = Joi.object({
  name: Joi.string(),
});

module.exports = {
  BrandCreationValidation,
  BrandUpdateValidation,
};
