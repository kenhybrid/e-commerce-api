import Joi from "@hapi/joi";

const ClientRegistrationValidation = Joi.object({
  // , tlds: { allow: ["com", "net"] }
  email: Joi.string().email({ minDomainSegments: 2 }).max(50).required(),
  name: Joi.string().max(30).required(),
  phone: Joi.string()
    .min(10)
    .max(12)
    .pattern(/^[0-9]+$/)
    .required(),
  password: Joi.string()
    .trim()
    .min(4)
    .max(16)
    .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
    .required(),
});

module.exports = {
  ClientRegistrationValidation,
};
