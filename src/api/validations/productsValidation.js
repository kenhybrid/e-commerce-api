import Joi from "@hapi/joi";

const ProductCreationValidation = Joi.object({
  name: Joi.string().required(),
  description: Joi.string().required(),
  brand: Joi.string().required(),
  category: Joi.string().required(),
  price: Joi.number().required(),
  quantity: Joi.number().required(),
});

const ProductUpdateValidation = Joi.object({
  name: Joi.string(),
  description: Joi.string(),
  brand: Joi.string(),
  category: Joi.string(),
  price: Joi.number(),
  quantity: Joi.number(),
});
const ProductFilterValidation = Joi.object({
  pricelow: Joi.number(),
  brand: Joi.string(),
  category: Joi.string(),
  pricehigh: Joi.number(),
});
const ProductSearchValidation = Joi.object({
  searchTerm: Joi.string()
    .lowercase()
    .trim()
    .regex(/[$\(\)<>]/, { invert: true }),
});

module.exports = {
  ProductCreationValidation,
  ProductFilterValidation,
  ProductUpdateValidation,
  ProductSearchValidation,
};
