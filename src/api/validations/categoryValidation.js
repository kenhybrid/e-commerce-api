import Joi from "@hapi/joi";

const CategoryCreationValidation = Joi.object({
  name: Joi.string().required(),
});

const CategoryUpdateValidation = Joi.object({
  name: Joi.string(),
});

module.exports = {
  CategoryCreationValidation,
  CategoryUpdateValidation,
};
