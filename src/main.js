import createApp from "fastify";
import "dotenv/config.js";
import mongoose from "./api/helpers/connection.js";
const app = createApp({ logger: true });
// plugins
// custom plugins
app.register(require("fastify-swagger"),{
  exposeRoute:true,
  routePrefix:"/docs",
  swagger:{
    info:{title:"ecommerce-api"}
  }
})
app.register(mongoose);
// decorators

// routes
app.get("/", async (req, res) => {
  res.send({ message: "Wellcome to my API" });
});
app.register(
  async (openRoutes) =>
    openRoutes.register(require("./api/routes/productsRoute.js")),
  { prefix: "/api/products" }
);
app.register(
  async (openRoutes) =>
    openRoutes.register(require("./api/routes/brandsRoute.js")),
  { prefix: "/api/brands" }
);
app.register(
  async (openRoutes) =>
    openRoutes.register(require("./api/routes/categoriesRoute.js")),
  { prefix: "/api/categories" }
);
// listen to app
const start = async () => {
  try {
    await app.ready();
    await app.listen("8000", "127.0.0.1");
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};
start();

export default app;
